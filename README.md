# **EYRINA4**
New version of motherboard for Eyrina platform. New platform for moving with camera has more motors and endstops. 

### New features
- Better integration (integrated stepper motor drivers)
- Onboard current supply withoutt external PCBs
- Drivers with more capabilities
- Absolute positioning (usage of endstops)
- Whole new protocol (based on Gcode) - Prefix E
- Communication via USB
- Protection before every possible human error (Dope resistant)

## **Motherboard features**
- 10 stepper motors vith motion engine and SPI interface
- Driver protection via diode field
- Endstop inputs for every motor
- USB protection via transil (USBLC6)


## **Stepper drivers - L6470**
- Configuration via SPI
- Chip select probably realized by shift registers
- Input from chip (FAULT) from every driver
- Reset pin is maybe only connect to VCC (not drivable)
- Endstop is connected directly to driver and can set home (mark) position, so motor can be positioned absolutly
    - second variant is connecting endstop to both (driver and MCU)
- Heatsink is necessary for driver, perhaps some long profile like [this]()


## **Input signals**
- Due to high count of inputs from must be mcu inputs extended
- Possible solutions are:
    - Matrix of wires, can take a lots of place on PCB, horible EMC
    - Coded via encoder, cannot sense more inputs than one per encoder
    - Multiplexor selection, lots of pins for control, slow scanning
    - Resistor ladder, non-standard resistor values for more than 4 inputs, need ADC, perhaps slow for more inputs
    - IO expander, communication via I2C or SPI, have built-in interrupt, price 40Kč, MPC23017
- IO expander is now probably the best choice


## **Power management**
- Input power supply voltage is 36V
- Stepdown regulator - possible is L5973D
- look at TPS54302 
- Set voltage by voltage divider: $V_{out} = \frac{R1+R2}{R2} * 1,235$

#### **Power consumption**
**36V**
- Motors 5x 1.2A per coil x 1.5 = 9A (324W)
- Some additional NEMA17 6A (180W)
- Lighting 1A (36W)
**12V**
- Fan 12V 0.5A (6W)
**5V & 3.3V**
- Logic only => 10W max

- Power supply shoud have 36V 20A

## **Temperature controll**
- 2x 4-pin fans
- temperature sensing via, MCU built-in sensor, ambient sensor (Si7055)
- two temperature [NTC sensors](https://www.tme.eu/en/details/135-104qad-j01/temperature-sensors-ntc/honeywell/) on stepper driver heatsinks
- Fan control is ensured by MCU or fan speed controller ic [EMC2305](https://cz.mouser.com/ProductDetail/Microchip-Technology/EMC2305-1-AP-TR?qs=pA5MXup5wxG44e0aDzIDlQ%3D%3D)

### GPIO Expander - MCP23017 (MCP23S17)
- Commucation via I2C (exist version with SPI)
- Add 16 GPIO in 2 ports
- TME I2C version [Link](https://www.tme.eu/cz/details/mcp23017-e_so/multiplexery-a-analogove-prepinace/microchip-technology/)
- TME I2C QFN package [Link](https://www.tme.eu/cz/details/mcp23017-e_ml/multiplexery-a-analogove-prepinace/microchip-technology/)
- TME SPI version, cheaper, perhaps fast [Link](https://www.tme.eu/cz/details/mcp23s17-e_so/multiplexery-a-analogove-prepinace/microchip-technology/)
- [Datasheet](doc/mcp23017.pdf)

## **Motors**
[Datasheet](doc/Stepper_motor_datasheet.pdf)
**Axis X**
Motor: Phytron VSS 32.200.1.2-HV (1.2A)
Screw: Right, diameter 8mm, pitch 0,5mm

**Axis Y**
Motor: Phytron VSS 32.200.1.2-HV (1.2A)
Screw: Right, diameter 8mm, pitch 0,5mm

**Axis Z**
Motor: Phytron VSS 42.200.1.2-VGPL 42/4-HV (gear 4:1) (1.2A)
Screw: Right, diameter 12mm, pitch 1mm

**Tilt**
Motor: Phytron VSS 42.200.1.2-VGPL 42/6-HV (gear 6:1) (1.2A)
Worm gear: 1:100

**Rotation**
Motor: Phytron VSS 25.200.1.2-HV (1.2A)
Worm gear: 1:180

### **Lightning**
- several LED in serial connection
- possible driver: [TPS92511](https://cz.mouser.com/ProductDetail/Texas-Instruments/TPS92511DDAR?qs=sGAEpiMZZMsE420DPIasPq8t8WQhxZ0Pvf3q6IURNAk%3d)
- input voltage: up to 65V
- switching frequency: 50-500kHz
- 3% current accuracy
. dimming via PWM signal
